const express = require('express');
const roomRouter = express.Router({});
const {sqlController} = require('../database/sqlController')

roomRouter.get('/get_all', async (req, res) => {
    const rooms = await sqlController.get("SELECT * FROM rooms;");
    res.status(rooms['statusCode']);
    res.json(rooms['data']);
});

roomRouter.get('/get_free', async(req, res) => {
    const sql = `SELECT id FROM rooms EXCEPT\
                (\
                    SELECT room_id FROM bookings WHERE start_period\
                    BETWEEN '${req.query['from']}' AND '${req.query['to']}'\
                    OR end_period  BETWEEN '${req.query['from']}' AND '${req.query['to']}'\
                );`
    const result =  await sqlController.get(sql);
    if (result['statusCode'] == 400) {
        console.log(result['message']);
    };
    res.status(result['statusCode']);
    res.json(result['data']);
});

roomRouter.post('/create', async (req, res) => {
    let sql = "INSERT INTO rooms (id) values ";
    for(let i = req.query['from']; i <= req.query['to']; i++) {
        sql += `(${i}),`
    };
    sql = sql.slice(0, -1) +  ";" 
    const createRoomsInDatabase =  await sqlController.create(sql);
    console.log("🚀 ~ file: index.js:23 ~ app.post ~ createRoomsInDatabase ", createRoomsInDatabase);
    res.status(createRoomsInDatabase['statusCode']);
    res.json(createRoomsInDatabase['message']);
});

module.exports = {roomRouter};