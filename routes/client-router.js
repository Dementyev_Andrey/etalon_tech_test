const express = require('express');
const clientRouter = express.Router({});
const {sqlController} = require('../database/sqlController')

clientRouter.post('/register', async (req, res) => {
    const result =  await sqlController.create(`INSERT INTO clients (name) VALUES ('${req.query['name']}');`);
    res.status(result['statusCode']);
    res.json(result['message']);
});

clientRouter.get('/get_all', async (req, res) => {
    const result =  await sqlController.get(`SELECT * FROM clients;`);
    res.status(result['statusCode']);
    res.json(result['data']);
});

clientRouter.delete('/remove/:id', async (req, res) => {
    const result =  await sqlController.delete(`DELETE FROM clients where id=${req.params['id']};`);
    res.status(result['statusCode']);
    res.json(result['message']);
});

module.exports = {clientRouter}