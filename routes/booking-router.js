const express = require('express');
require('dotenv').config();
const bookingRouter = express.Router({});
const axios = require('axios');
const {sqlController} = require('../database/sqlController')


bookingRouter.get('/get', async (req, res) => {
    const bookings = await sqlController.get("SELECT * FROM bookings;");
    res.status(bookings['statusCode']);
    res.json(bookings['data']);
});

bookingRouter.post('/to_book', async (req, res) => {
    const checkTimeSlotSql = `SELECT id FROM bookings WHERE\
                            start_period BETWEEN '${req.body['dateFrom']}' AND '${req.body['dateTo']}'\
                            OR 
                            end_period  BETWEEN '${req.body['dateFrom']}' AND '${req.body['dateTo']}';`
    const isFree =  await sqlController.get(checkTimeSlotSql);
    if (!(req.body['roomId'] in isFree['data'])) {
        let isVip;
        await axios.get(`http://${process.env.HELPER_HOST}:3002/`)
            .then(function (response) {
                isVip = response['data']['VIP'];
            })
            .catch(function (error) {
                console.log(error);
            });

        let sql = `INSERT INTO bookings (client_id, room_id, start_period,end_period, vip) 
                    VALUES (\
                        ${req.body['clientId']},\
                        ${req.body['roomId']},\
                        '${req.body['dateFrom']}',\
                        '${req.body['dateTo']}',\
                        '${isVip}'\
                    );`
        const result =  await sqlController.create(sql);
        console.log("🚀 ~ file: index.js:61 ~ app.post ~ result:", result['message']);
        res.status(result['statusCode']);
        res.json(result['message']);
    } else {
        res.status(400);
        res.json({'message':'timeslot not free'});
    };
});

bookingRouter.delete('/remove',  async (req, res) => {
    const sql = `DELETE FROM bookings WHERE\
                client_id=${req.body['clientID']}\
                AND room_id = ${req.body['roomID']}\
                AND start_period = '${req.body['startPeriod']}'\
                AND end_period = '${req.body['endPeriod']}';`
    const result = await sqlController.delete(sql);
    res.status(result['statusCode']);
    if (result['statusCode'] == 400) {
        console.log(result['message']);
    };
    res.json(result['message']);
});

module.exports = {bookingRouter};