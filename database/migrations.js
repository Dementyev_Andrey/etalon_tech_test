const {sqlController} = require("./sqlController");

const migrate  = async () => {
    let createTableClients = await sqlController.create( 
        "CREATE TABLE IF NOT EXISTS clients (\
            id serial PRIMARY KEY ,\
            name varchar(255) NOT NULL\
        );"
    );
    console.log("🚀 ~ file: migrations.js:11 ~ migrate ~ createTableClients:", createTableClients)

    let createTableRooms = await sqlController.create( 
        "CREATE TABLE IF NOT EXISTS rooms (id serial PRIMARY KEY);"
    );
    console.log("🚀 ~ file: migrations.js:16 ~ migrate ~ createTableRooms:", createTableRooms)
    
    let createTableBookings = await sqlController.create( 
        "CREATE TABLE IF NOT EXISTS bookings (\
            id serial PRIMARY KEY ,\
            client_id INT,\
            room_id INT,\
            start_period TIMESTAMP  NOT NULL,\
            end_period TIMESTAMP  NOT NULL,\
            FOREIGN KEY(client_id) REFERENCES clients (id),\
            FOREIGN KEY(room_id) REFERENCES rooms (id),\
            VIP boolean\
        );"
    );
    console.log("🚀 ~ file: migrations.js:28 ~ migrate ~ createTableBookings:", createTableBookings)
};

module.exports = {migrate}