require('dotenv').config()

var pgp = require("pg-promise")(/*options*/);

class SqlController {
    constructor() {
        this.POSTGRES_USER = process.env.POSTGRES_USER;
        this.POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD;
        this.POSTGRES_HOST = process.env.POSTGRES_HOST;
        this.POSTGRES_PORT = process.env.POSTGRES_PORT;
        this.POSTGRES_DB = process.env.POSTGRES_DB;
        this.response = null;
    };
    
    connection () {
        return pgp(`postgres://${ this.POSTGRES_USER}:${this.POSTGRES_PASSWORD}@${this.POSTGRES_HOST}:${ this.POSTGRES_PORT}/${this.POSTGRES_DB}`)
    };

    async create(sql) {
        let db = this.connection()
        await db.query(sql)
            .then(() => {
                this.response = {
                            'succesfull': true,
                            'statusCode': 201,
                            'message': 'created'
                };
            })
            .catch((error) => {
                this.response =  {
                            'succesfull': false,
                            'statusCode': 400,
                            'message': error
                };
            });
        db.$pool.end();
        return this.response
    };

    async get(sql) {
        let db = this.connection()
        await db.any(sql)
        .then((result) => {
            this.response = {
                'succesfull': true,
                'statusCode': 200,
                'data': result
            };
        })
        .catch((error) => {
            this.response =  {'succesfull': false,
                'statusCode': 400,
                'message': error
            };
        });
        db.$pool.end();
        return this.response
    };

    async delete(sql) {
        let db = this.connection()
        await db.query(sql)
        .then(() => {
            this.response = {
                'succesfull': true,
                'statusCode': 200,
                'message': "removed"
            };
        })
        .catch((error) => {
            this.response =  {'succesfull': false,
                'statusCode': 400,
                'message': error
            };
        });
        db.$pool.end();
        return this.response
    };
    
};
sqlController = new SqlController();

module.exports = {sqlController};