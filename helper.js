const express = require('express')
const app = express()
const port = 3002
app.use(express.json())

app.get('/', (req, res) => {
    console.log()
    res.status(200);
    res.json({"VIP": Boolean(Math.floor(Math.random() * 2))})
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
