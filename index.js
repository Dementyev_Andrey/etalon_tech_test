const express = require('express');
const {migrate} = require("./database/migrations");

const {clientRouter} = require("./routes/client-router")
const {roomRouter} = require("./routes/room-router")
const {bookingRouter} = require("./routes/booking-router")

const app = express();


const port = 3001;

app.use(express.json())

app.use('/client', clientRouter);
app.use('/room', roomRouter);
app.use('/booking', bookingRouter);


app.listen(port, async () => {
    await migrate();
    console.log(`Example app listening on port ${port}`);
});

